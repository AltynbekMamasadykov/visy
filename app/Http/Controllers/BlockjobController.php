<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blockjob;
use App\Models\Task;
use Auth;

class BlockjobController extends Controller
{
    public function show($id)
    {   
        $blockjob = Blockjob::with('tasks')->findOrFail($id);
        // dd($blockjob);
        return view('blockjobs.shows', compact('blockjob'));
    }

    public function activate($id)
    {   
        $blockjob = Blockjob::findOrFail($id);
        // session()->flash('message', 'Blockjob '.$blockjob->name. '  has been activated');
        return view('blockjobs.activate',compact('blockjob'));
    }

    public function storeactivate(Request $request, $id)
    {   
        $blockjob = Blockjob::findOrFail($id);
        // dd($blockjob);
        $data = request()->validate([
            'start_date' => ['date'],      
            'deadline' => ['date'],      
        ]);
        // dd($data);
        $blockjob->start_date = $data['start_date'];
        $blockjob->deadline = $data['deadline'];
        $blockjob->update();
                
        $task = new Task;
        $task->blockjob_id = $blockjob->id;
        $task->body = 'Lets Start';
        $task->user = Auth::user()->name;        
        $task->save();
        
        return redirect()->route('blockjobs.show', ['id'=> $id])
        ->with('message', 'Blockjob '.$blockjob->name. '  has been activated');
    }
}
