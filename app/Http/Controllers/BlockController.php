<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Block;
use App\Models\Project;
use App\Models\Job;
use App\Models\Subjob;
use App\Models\Blockjob;
use DB;
use Auth;

class BlockController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    
    public function index()
    {
        $blocks = Block::all();
        return view('blocks.index',compact('blocks'));
    }

    public function create()
    {
        $projects=Project::all();
        return view('blocks.create',compact('projects'));
    }

    public function store(Request $request)
    {
        $project_id = $request->project;
        $project = Project::findOrFail($project_id);
        // dd($project);
        $data = request()->validate([
            'name' => ['required', 'string', 'max:255'],      
            'etajnost' => ['required', 'integer'],      
        ]);
        $block = new Block;
        $block->name = $data['name'];
        $block->etajnost = $data['etajnost'];
        $block->project_id = $request->project;
        $block->project_name = $project->name;
        $block->save();
        
        $subjobs = Subjob::where('kratnost','1')->get();
        foreach ($subjobs as $subjob){
            $blockjob = new Blockjob;
            $blockjob->block_id  = $block->id;
            $blockjob->block_name  = $block->name;
            $blockjob->job_id  = $subjob->job_id;
            $blockjob->job_name  = $subjob->job_name;
            $blockjob->subjob_id  = $subjob->id;
            $blockjob->subjob_name  = $subjob->name;
            $blockjob->roles  = $subjob->roles;
            $blockjob->save();}

            for($i = 1; $i<=($block->etajnost);$i++)
            {
                $subjobs = Subjob::where('kratnost','n')->get();
                foreach ($subjobs as $subjob){
                    $blockjob = new Blockjob;
                    $blockjob->block_id  = $block->id;
                    $blockjob->block_name  = $block->name;
                    $blockjob->job_id  = $subjob->job_id;
                    $blockjob->job_name  = $subjob->job_name;
                    $blockjob->subjob_id  = $subjob->id;
                    $blockjob->etaj  = $i;
                    $blockjob->subjob_name  = $subjob->name;
                    $blockjob->roles  = $subjob->roles;
                    $blockjob->save();}
            }
        session()->flash('message', 'Block '.$block->name. '  has been created');
        return redirect()->route('blocks.index');
    }

    public function edit($id)
    {
        $projects=Project::all();
        $block = Block::findOrFail($id);
        return view ('blocks.edit', compact('block','projects'));
    }

    public function update(Request $request, $id)
    {
        $block = Block::findOrFail($id);
        
        $data = request()->validate([
            'name' => ['required', 'string', 'max:255'], 
            'etajnost' => ['required', 'integer'],
        ]);
        $block->name = $data['name'];
        $block->etajnost = $data['etajnost'];            
        $block->update();

        DB::table('blockjobs')->where('block_id',$id)->update(
            ['block_name' => $block->name,]
            );
        
        session()->flash('message', 'Block '.$block->name. '  has been updated');
        return redirect()->route('blocks.index');
    }

    public function addsubjobform($id)
    {
        // dd($id);
        $block = Block::findOrFail($id);
        $jobs = Job::all();
        $subjobs = Subjob::all();
        return view('blocks.addsubjobform',compact('block','jobs','subjobs'));
    }

    public function addsubjob(Request $request)
    {
        $block = Block::findOrFail($request->id);
        $job = Job::findOrFail($request->job);        
        $subjob = Subjob::findOrFail($request->subjob);
        $isExist = DB::table('blockjobs')->where('subjob_id',$subjob->id)->get();
        // dd($isExist);
        // check if subjob is already assigned to this block
        if ($isExist->isNotEmpty()) {
            return redirect()->route('blocks.index')->with('message', 'this subjob already exits in this block');   
        }

        else if ($subjob->kratnost == '1') 
            {
                $blockjob = new Blockjob;
                $blockjob->block_id  = $block->id;
                $blockjob->block_name  = $block->name;
                $blockjob->job_id  = $subjob->job_id;
                $blockjob->job_name  = $subjob->job_name;
                $blockjob->subjob_id  = $subjob->id;
                $blockjob->subjob_name  = $subjob->name;
                $blockjob->roles  = $subjob->roles;
                $blockjob->save();
            }
        
        else 
        for($i = 1; $i<=($block->etajnost);$i++)
            {
                $blockjob = new Blockjob;
                $blockjob->block_id  = $block->id;
                $blockjob->block_name  = $block->name;
                $blockjob->job_id  = $subjob->job_id;
                $blockjob->job_name  = $subjob->job_name;
                $blockjob->subjob_id  = $subjob->id;
                $blockjob->etaj  = $i;
                $blockjob->subjob_name  = $subjob->name;
                $blockjob->roles  = $subjob->roles;
                $blockjob->save();
            }
        session()->flash('message', 'Subjob '.$subjob->name. '  has been added');
        return redirect()->route('blocks.index');
    }

    public function show($id)
    {
        $block = Block::where('id',$id)->first();
        $blockjobs = Blockjob::where('block_id', $id)->paginate(20);
        
        // $job_id = $blockjobs[0]->job_id;
        // $var = DB::table('job_role')->where('job_id', $job_id)->select('role_id')->get()->toArray();
        // $array=[];
        // foreach($var as $q){
        //     $array[] = $q->role_id;
        // }
        // $w = 1;
        // if(in_array($w, $array)){
        //     return 'yes';
        // }else{
        //     return 'no';
        // }
        // dd($blockjob);
        // foreach ($blockjobs as $blockjob) {
        // $var =  DB::table('job_role')->where('job_id',$blockjob->job_id)->select('role_id')->get()->toArray();
        // // dd($var);
        // $array=[];
        // foreach ($var as $va) { $array[] = $va->role_id;}
        // if(in_array(Auth::user()->role->id, $array)){
        //         return 'yes';
        //     }else{
        //         return 'no';
        //     }
        // }
        
        $userrole =(Auth::user()->role->name);
        
        return view('blocks.show',compact('block','blockjobs','userrole'));
    }

    
}
