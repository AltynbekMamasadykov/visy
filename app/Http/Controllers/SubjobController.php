<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Job;
use App\Models\Subjob;
use DB;

class SubjobController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','admin']);
    }
    
    public function index()
    {
        $subjobs = Subjob::orderBy('job_id','asc')->paginate(20);
        return view('subjobs.index',compact('subjobs'));
    }

    public function create()
    {
        $jobs=Job::all();
        return view('subjobs.create',compact('jobs'));
    }

    public function store(Request $request)
    {   
        // dd($request);
        $job = Job::findOrFail($request->job);
        $job_name = $job->name;
        $data = request()->validate([
            'name' => ['required', 'string', 'max:255'],      
            'kratnost' => ['required', 'string', 'max:255'],      
        ]);
        $subjob = new Subjob;
        $subjob->name = $data['name'];
        $subjob->kratnost = $data['kratnost'];
        $subjob->job_id = $request->job;        
        $subjob->job_name = $job_name;        
        $subjob->roles = $job->roles;        
        $subjob->save();   
        session()->flash('message', 'Subjob '.$subjob->name. '  has been created');         
        return redirect()->route('subjobs.index');
    }

    public function edit($id)
    {
        $subjob = Subjob::findOrFail($id);
        $jobs = Job::all();
        return view ('subjobs.edit', compact('subjob','jobs'));
    }

    public function update(Request $request, $id)
    {
        // dd($request->kratnost);
        $job = Job::findOrFail($request->job);
        $job_name = $job->name;
        $subjob = Subjob::findOrFail($id);        
        $data = request()->validate([
            'name' => ['required', 'string', 'max:255'],      
        ]);
        $subjob->name = $data['name']; 
        $subjob->job_id = $request->job;        
        $subjob->job_name = $job_name; 
        if ($request->filled('kratnost'))
            {$request->validate(['kratnost' => 'required', 'string', 'max:255']);
                $subjob->kratnost = $request['kratnost'];}           
        $subjob->update();

        DB::table('blockjobs')->where('subjob_id',$id)->update(
            ['subjob_name' => $subjob->name,]
            );
        session()->flash('message', 'Subjob '.$subjob->name. '  has been updated');
        return redirect()->route('subjobs.index');
    }

    public function test($id)
    {
        dd($id);
        $var = DB::table('block_subjob')->where('id',$id)->first();        
        return view ('subjobs.test',compact('var'));
    }
}
