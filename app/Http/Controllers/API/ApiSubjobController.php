<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Job;
use App\Models\Subjob;
use DB;

class ApiSubjobController extends Controller
{
    public function index()
    {
        $subjobs = Subjob::orderBy('job_id','asc')->get();
        return response()->json(['subjobs' => $subjobs]);         
    }

    public function store(Request $request)
    {
        $job = Job::findOrFail($request->job_id);
        $job_name = $job->name;
        $data = $request->all();
        $validator = \Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],      
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()], 400);
        }

        $subjob = new Subjob;
        $subjob->name = $data['name'];
        $subjob->job_id = $request->job_id;        
        $subjob->job_name = $job_name;        
        $subjob->roles = $job->roles;        
        $subjob->save();            
        return response()->json(["message" => "Subjob created"], 201);
    }

    public function update(Request $request, $id)
    {
        $job = Job::findOrFail($request->job_id);
        $job_name = $job->name;
        $subjob = Subjob::findOrFail($id);        
        $data = $request->all();
        $validator = \Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],      
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()], 400);
        }
        $subjob->name = $data['name']; 
        $subjob->job_id = $request->job_id;        
        $subjob->job_name = $job_name;            
        $subjob->update();

        DB::table('blockjobs')->where('subjob_id',$id)->update(
            ['subjob_name' => $subjob->name,]
            );        
            return response()->json(["message" => "Subjob updated"], 201);
    }

}
