<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blockjob;
use App\Models\Task;



class ApiBlockjobController extends Controller
{
    public function show($id)
    {   
        $blockjob = Blockjob::findOrFail($id);
        return response()->json(['blockjob' => $blockjob]);
        
    }
    
    public function storeactivate(Request $request, $id)
    {   
        $blockjob = Blockjob::findOrFail($id);
        
        $data = $request->all();
        $validator = \Validator::make($request->all(), [
            'start_date' => ['date'],      
            'deadline' => ['date'],                 
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()], 400);
        }
        
        $blockjob->start_date = $data['start_date'];
        $blockjob->deadline = $data['deadline'];
        $blockjob->update();

        $task = new Task;
        $task->blockjob_id = $blockjob->id;
        $task->body = 'Lets Start';
        $task->user = auth('api')->user()->name;        
        $task->save();
        
        return response()->json(["message" => "Blockjob activated"], 201);
        
    }
}
