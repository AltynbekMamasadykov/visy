<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use Illuminate\Validation\Rule;


class ApiUserController extends Controller
{    
    public function index()
    {
        $users = User::with('role')->where('id','!=', 1)->get();
        return response()->json(['users' => $users]); 
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = \Validator::make($request->all(), [
            'email' => ['required', 'email', 'max:255', 'unique:users'],
            'name' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'role_id' =>['required']
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()], 400);
        }
        
        $user = new User();
        $user->email = $data['email'];
        $user->name = $data['name'];
        $user->password = bcrypt($data['password']);        
        $user->role_id = $data['role_id'];
        $user->save();            

        return response()->json(["message" => "user created"], 201);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $user = User::findOrFail($id);
        $validator = \Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],            
            'email' => ['required', Rule::unique('users')->ignore($user->id)],
            'role_id' =>['required']
            
        ]);
        $user->name = $data['name'];        
        $user->email = $data['email'];
        $user->role_id = $data['role_id'];
        // $user->role_id = implode(' ', $request->roles); 
        if ($request->filled('password')){$request->validate(['password' => 'min:8', 'confirmed']);$user->password = bcrypt($request['password']);} 
        $user->update();     
        
        return response()->json(["message" => "user updated"], 201);
    }

    // public function destroy($id)
    // {
    //     $user = User::findOrFail($id);
    //     $user->roles()->detach();        
    //     $user->delete();
    //     return response()->json(["message" => "user deleted" ], 202);
    // }  
    
}