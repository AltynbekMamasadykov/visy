<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Block;

class ApiProjectController extends Controller
{
    public function index()
    {
        $projects = Project::all();
        return response()->json(['projects' => $projects]); 
    }

    public function store(Request $request)
    {   
        $data = $request->all();
        $validator = \Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],                
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()], 400);
        }
        
        $project = new Project();
        $project->name = $data['name'];
        $project->save();    
        return response()->json(["message" => "project created"], 201);
    }

    public function show($id)
    {
        $blocks = Block::where('project_id',$id)->get();
        return response()->json(['blocks' => $blocks]); 
    }

    public function update(Request $request, $id)
    {
        $project = Project::findOrFail($id);
        $data = $request->all();
        $validator = \Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],                
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()], 400);
        }
        
        $project->name = $data['name'];        
        $project->update();        
        return response()->json(["message" => "project updated"], 201);
    }
}
