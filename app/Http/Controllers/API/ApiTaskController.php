<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blockjob;
use App\Models\Task;

class ApiTaskController extends Controller
{
    
    public function store(Request $request, $id)
    {
        $blockjob = Blockjob::findOrFail($id);
        
        $data = $request->all();
        $validator = \Validator::make($request->all(), [
            'body' => ['string'],                           
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()], 400);
        }        
        
        $task = new Task;
        $task->blockjob_id = $blockjob->id;
        $task->body = $data['body'];
        $task->user = auth('api')->user()->name;
        
        $task->save();
        
        return response()->json(["message" => "task created"], 201);
        
    }

    
    public function update(Request $request, $id)
    {
        $task = Task::findOrFail($id);

        $data = $request->all();
        $validator = \Validator::make($request->all(), [
            'body' => ['string'],                           
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()], 400);
        }
        
        $task->body = $data['body'];
        $task->update();
        // $blockjob_id = $task->blockjob_id;     
        
        return response()->json(["message" => "task updated"], 201);
    }

}
