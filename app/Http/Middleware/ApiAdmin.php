<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ApiAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth('api')->user() && auth('api')->user()->id == 1) {
        // if (auth()->user() && auth()->user()->id == 1) {
            return $next($request);
            }
        return response()->json(['message' => 'not found Admin emes ekensingo'], 404);
        // abort(404);
    }
}
