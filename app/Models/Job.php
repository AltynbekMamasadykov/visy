<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Subjob;
use App\Models\Role;
use App\Models\Block;

class Job extends Model
{
    use HasFactory;

    // protected $casts = ['roles' => 'array'];

    public function subjobs()
    {
        return $this->hasMany(Subjob::class);
        
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    // public function blocks()
    // {   
        
    //     return $this->belongsToMany(Block::class)
    //     ->withPivot('id','status');
    // }
    
}
