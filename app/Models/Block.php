<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Project;
use App\Models\Job;

class Block extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        
    ];

    public function projects()
    {
        return $this->belongsTo(Project::class);        
    }

    // public function jobs()
    // {
    //     return $this->belongsToMany(Job::class)
    //     ->withPivot('id','status');
    // }
}
