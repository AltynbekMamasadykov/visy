<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Subjob;

class SubjobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [            
            ['id' => 1,  'job_id' => 1, 'job_name' => 'kotlovan', 'kratnost' => '1', 'name' => 'razbivka','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 2,  'job_id' => 1, 'job_name' => 'kotlovan', 'kratnost' => '1','name' => 'zem raboty','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 3,  'job_id' => 1, 'job_name' => 'kotlovan', 'kratnost' => '1','name' => 'akt priemki','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 4,  'job_id' => 2, 'job_name' => 'fundament', 'kratnost' => '1','name' => 'podbetonka','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 5,  'job_id' => 2, 'job_name' => 'fundament', 'kratnost' => '1','name' => 'armokarkasnie raboty','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 6,  'job_id' => 2, 'job_name' => 'fundament', 'kratnost' => '1','name' => 'ukladka betona','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 7,  'job_id' => 2, 'job_name' => 'fundament', 'kratnost' => '1','name' => 'obratnaya zasipka','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 8,  'job_id' => 2, 'job_name' => 'fundament', 'kratnost' => '1','name' => 'pol v podval','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 9,  'job_id' => 2, 'job_name' => 'fundament', 'kratnost' => '1','name' => 'akt priemki','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 10, 'job_id' => 3, 'job_name' => 'karkas zdaniya','kratnost' => 'n','name' => 'podporniye steni','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 11, 'job_id' => 3, 'job_name' => 'karkas zdaniya','kratnost' => 'n','name' => 'kolonny','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 12, 'job_id' => 3, 'job_name' => 'karkas zdaniya','kratnost' => 'n','name' => 'diafragmy','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 13, 'job_id' => 3, 'job_name' => 'karkas zdaniya','kratnost' => 'n','name' => 'plita perekritiya','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 14, 'job_id' => 3, 'job_name' => 'karkas zdaniya','kratnost' => 'n','name' => 'lestnitsy','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 15, 'job_id' => 3, 'job_name' => 'karkas zdaniya','kratnost' => 'n','name' => 'rigel','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 16, 'job_id' => 3, 'job_name' => 'karkas zdaniya','kratnost' => 'n','name' => 'balki','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 17, 'job_id' => 3, 'job_name' => 'karkas zdaniya','kratnost' => '1','name' => 'gorizontalnya gidroizolyatcia fundamenta','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 18, 'job_id' => 3, 'job_name' => 'karkas zdaniya','kratnost' => '1','name' => 'vertikalnaya gidroizolyatcia fundamenta','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 19, 'job_id' => 3, 'job_name' => 'karkas zdaniya','kratnost' => '1','name' => 'akt priemki','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 20, 'job_id' => 4, 'job_name' => 'steny','kratnost' => 'n','name' => 'montaj met kontrukciy','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 21, 'job_id' => 4, 'job_name' => 'steny','kratnost' => 'n','name' => 'kirpicnaya kladka','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 22, 'job_id' => 4, 'job_name' => 'steny','kratnost' => 'n','name' => 'gazoblok','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 23, 'job_id' => 4, 'job_name' => 'steny','kratnost' => 'n','name' => 'vent shahty montaj diflektorov','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 24, 'job_id' => 4, 'job_name' => 'steny','kratnost' => 'n','name' => 'akt priemki','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 25, 'job_id' => 5, 'job_name' => 'krovlya','kratnost' => '1','name' => 'parapet','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 26, 'job_id' => 5, 'job_name' => 'krovlya','kratnost' => '1','name' => 'paraizolyatcia','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 27, 'job_id' => 5, 'job_name' => 'krovlya','kratnost' => '1','name' => 'utepleniye krovli','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 28, 'job_id' => 5, 'job_name' => 'krovlya','kratnost' => '1','name' => 'progoni','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 29, 'job_id' => 5, 'job_name' => 'krovlya','kratnost' => '1','name' => 'stoyki','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 30, 'job_id' => 5, 'job_name' => 'krovlya','kratnost' => '1','name' => 'stropily','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 31, 'job_id' => 5, 'job_name' => 'krovlya','kratnost' => '1','name' => 'obreshetka','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 32, 'job_id' => 5, 'job_name' => 'krovlya','kratnost' => '1','name' => 'krovlya','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 33, 'job_id' => 5, 'job_name' => 'krovlya','kratnost' => '1','name' => 'vnutrenniy vodostok','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 34, 'job_id' => 5, 'job_name' => 'krovlya','kratnost' => '1','name' => 'akt priemki','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 35, 'job_id' => 6, 'job_name' => 'fasad','kratnost' => '1','name' => 'utepleniye narujnih sten','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 36, 'job_id' => 6, 'job_name' => 'fasad','kratnost' => '1','name' => 'otdelka','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 37, 'job_id' => 6, 'job_name' => 'fasad','kratnost' => '1','name' => 'pokraska','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 38, 'job_id' => 6, 'job_name' => 'fasad','kratnost' => '1','name' => 'montaj livnevih trub','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 39, 'job_id' => 6, 'job_name' => 'fasad','kratnost' => '1','name' => 'akt priemki','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 40, 'job_id' => 7, 'job_name' => 'lift','kratnost' => '1','name' => 'montaj metalicheskogo karkasa shahti lifta','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 41, 'job_id' => 7, 'job_name' => 'lift','kratnost' => '1','name' => 'montaj liftov','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 42, 'job_id' => 7, 'job_name' => 'lift','kratnost' => '1','name' => 'ispytaniye','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 43, 'job_id' => 7, 'job_name' => 'lift','kratnost' => '1','name' => 'vvod','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 44, 'job_id' => 7, 'job_name' => 'lift','kratnost' => '1','name' => 'akt priemki','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 45, 'job_id' => 8, 'job_name' => 'okna i dveri','kratnost' => 'n','name' => 'montaj okon i dverey','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 46, 'job_id' => 8, 'job_name' => 'okna i dveri','kratnost' => 'n','name' => 'ispitaniya na shumoizolyaciyu','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 47, 'job_id' => 8, 'job_name' => 'okna i dveri','kratnost' => 'n','name' => 'akt priemki','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 48, 'job_id' => 9, 'job_name' => 'vnutrennaya otdelka','kratnost' => 'n','name' => 'mokriy process','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 49, 'job_id' => 9, 'job_name' => 'vnutrennaya otdelka','kratnost' => 'n','name' => 'otdelka podyezdov','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 50, 'job_id' => 9, 'job_name' => 'vnutrennaya otdelka','kratnost' => 'n','name' => 'pokraska','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 51, 'job_id' => 9, 'job_name' => 'vnutrennaya otdelka','kratnost' => 'n','name' => 'otdelka komerceskih pomesheniy','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 52, 'job_id' => 9, 'job_name' => 'vnutrennaya otdelka','kratnost' => 'n','name' => 'montaj peril','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 53, 'job_id' => 9, 'job_name' => 'vnutrennaya otdelka','kratnost' => 'n','name' => 'montaj balkonnih peril','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 54, 'job_id' => 9, 'job_name' => 'vnutrennaya otdelka','kratnost' => 'n','name' => 'akt priemki','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 55, 'job_id' => 10, 'job_name' => 'santehnika','kratnost' => 'n','name' => 'vodosnabjenie','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 56, 'job_id' => 10, 'job_name' => 'santehnika','kratnost' => 'n','name' => 'schetchik vodosnabjenie','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 57, 'job_id' => 10, 'job_name' => 'santehnika','kratnost' => 'n','name' => 'kanalizacia','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 58, 'job_id' => 10, 'job_name' => 'santehnika','kratnost' => 'n','name' => 'opressovka i ispytanie','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 59, 'job_id' => 10, 'job_name' => 'santehnika','kratnost' => 'n','name' => 'akt priemki','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 60, 'job_id' => 10, 'job_name' => 'santehnika','kratnost' => 'n','name' => 'vvod','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 61, 'job_id' => 11, 'job_name' => 'elektrosnabjenie','kratnost' => 'n','name' => 'narujnaya set (liniya)','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 62, 'job_id' => 11, 'job_name' => 'elektrosnabjenie','kratnost' => 'n','name' => 'ustanovka transformatora','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 63, 'job_id' => 11, 'job_name' => 'elektrosnabjenie','kratnost' => 'n','name' => 'podklyuchenie kvartir','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 64, 'job_id' => 11, 'job_name' => 'elektrosnabjenie','kratnost' => 'n','name' => 'ustanovka schetchikov','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 65, 'job_id' => 11, 'job_name' => 'elektrosnabjenie','kratnost' => 'n','name' => 'montaj lestnic i koridorov','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 66, 'job_id' => 11, 'job_name' => 'elektrosnabjenie','kratnost' => 'n','name' => 'ispitanie','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 67, 'job_id' => 11, 'job_name' => 'elektrosnabjenie','kratnost' => 'n','name' => 'registracia v energosbyte','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 68, 'job_id' => 11, 'job_name' => 'elektrosnabjenie','kratnost' => 'n','name' => 'akt priemki','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 69, 'job_id' => 12, 'job_name' => 'gazosnabjenie','kratnost' => 'n','name' => 'podklyucenie kvartir k gazosnabjeniyu','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 70, 'job_id' => 12, 'job_name' => 'gazosnabjenie','kratnost' => 'n','name' => 'narujniy montaj trub','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 71, 'job_id' => 12, 'job_name' => 'gazosnabjenie','kratnost' => 'n','name' => 'ustanovka schetchikov','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 72, 'job_id' => 12, 'job_name' => 'gazosnabjenie','kratnost' => 'n','name' => 'ispytanie','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 73, 'job_id' => 12, 'job_name' => 'gazosnabjenie','kratnost' => 'n','name' => 'registraciya v gazprom','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 74, 'job_id' => 12, 'job_name' => 'gazosnabjenie','kratnost' => 'n','name' => 'akt priemki','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 75, 'job_id' => 13, 'job_name' => 'otoplenie','kratnost' => '1','name' => 'proekt','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 76, 'job_id' => 13, 'job_name' => 'otoplenie','kratnost' => '1','name' => 'soglasovaniye','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 77, 'job_id' => 13, 'job_name' => 'otoplenie','kratnost' => '1','name' => 'expertiza','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 78, 'job_id' => 13, 'job_name' => 'otoplenie','kratnost' => '1','name' => 'montaj','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 79, 'job_id' => 13, 'job_name' => 'otoplenie','kratnost' => '1','name' => 'akt priemki','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 80, 'job_id' => 14, 'job_name' => 'blagoustroystvo','kratnost' => '1','name' => 'betonnya styajka','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 81, 'job_id' => 14, 'job_name' => 'blagoustroystvo','kratnost' => '1','name' => 'bruscatka','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 82, 'job_id' => 14, 'job_name' => 'blagoustroystvo','kratnost' => '1','name' => 'bordyur','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 83, 'job_id' => 14, 'job_name' => 'blagoustroystvo','kratnost' => '1','name' => 'lotok','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 84, 'job_id' => 14, 'job_name' => 'blagoustroystvo','kratnost' => '1','name' => 'detskaya ploshadka','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 85, 'job_id' => 14, 'job_name' => 'blagoustroystvo','kratnost' => '1','name' => 'ozelenenie','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],
            ['id' => 86, 'job_id' => 14, 'job_name' => 'blagoustroystvo','kratnost' => '1','name' => 'akt priemki','roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])],

        ];
    
        foreach ($items as $item) {
            Subjob::create($item);
        }
    }
}
