@extends('layouts.app')

@section('content')

    @if (session()->has('message'))
        <div id="alert" class="text-white px-6 py-4 border-0 rounded relative mb-4 bg-green-500 bg-opacity-75">
            <span class="inline-block align-middle mr-8">
                {{ session('message') }}
            </span>
            <button class="absolute bg-transparent text-2xl font-semibold leading-none right-0 top-0 mt-4 mr-6 outline-none focus:outline-none" onclick="document.getElementById('alert').remove();">
                <span>×</span>
            </button>
        </div>
    @endif

    <div class ="flex flex-wrap justify-end text-center mb-6"> 
        <a href="{{ route('jobs.create')}}">
            <button class="flex mx-auto mt-6 text-white bg-blue-500 border-0 py-2 px-5 focus:outline-none hover:bg-indigo-600 rounded">Create Job</button>
        </a>
    </div>
    <div class="flex justify-center">
        <table class="shadow-lg bg-white">
        <thead>
        <tr>
            <th class="bg-blue-500 bg-opacity-75 border text-left px-8 py-4">Jobs</th>
            <th class="bg-blue-500 bg-opacity-75 border text-left px-8 py-4">Roles</th>
            <th class="bg-blue-500 bg-opacity-75 border text-left px-8 py-4">Actions</th>
            
        </tr>
        </thead>
        <tbody>
        @foreach ($jobs as $job)
        <tr>            
            <td class="border px-4 py-2">{{ $job->name }}</td>
            <td class="border px-4 py-2">{{$job->roles}} </td>
            <td class="border px-4 py-2">
                <a href="{{ route ('jobs.edit', $job->id ) }}"> 
                <button type="button" class="bg-blue-500 text-white px-2 py-1
                    rounded font-medium w-full focus:outline-none hover:bg-indigo-600 rounded">Edit</button>
                </a>
            </td>
        </tr>
        </tbody>
        @endforeach     
        </table>
    </div>

@endsection
