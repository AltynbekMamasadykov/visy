@extends('layouts.app')

@section('content')

<div class="flex justify-center">
    <div class="w-4/12 bg-white p-6 rounden-lg">
        @if (session('status'))
            <div class="bg-red-500 p-4 rounded-lg mb-6 text-white text-center">
                {{ session('status') }}
            </div>
        @endif

        <div class="mb-4 text-center">
                
        CREATE TASK for job
                
        </div>   

        <form action="{{ route ('tasks.store', $blockjob->id) }}" method="post">
            @csrf
            
            {{$blockjob->block_name}} <br>
            {{$blockjob->job_name}} <br>
            {{$blockjob->subjob_name}} <br>

            <div class="mb-4">
                <label for="body" class="sr-only"> Task </label>
                <input type="text" name="body" id="body" placeholder="enter task"
                class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('task') border-red-500 @enderror" value="{{ old('task') }}">
                @error('name')
                    <div class="text-red-500 mt-2 text-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div>       
            
            
            <div>
                <button type="submit" class="bg-blue-500 text-white px-4 py-3
                rounded font-medium w-full focus:outline-none hover:bg-indigo-600 rounded"> Create </button>
            </div>           
        </form>
    </div>
</div>

@endsection
