@extends('layouts.app')

@section('content')

<div class="flex justify-center">
    <div class="w-4/12 bg-white p-6 rounden-lg">  
        @if (session('status'))
            <div class="bg-red-500 p-4 rounded-lg mb-6 text-white text-center">
                {{ session('status') }}
            </div>
        @endif

        <div class="mb-4 text-center">
                
        EDIT USER
                
        </div>

        <form action="{{ route('admin.users.update', $user ?? '') }} " method="post">
            @csrf
            @method('PUT')
            
            <div class="mb-4">
                <label for="name" class="sr-only"> Name </label>
                <input type="text" name="name" id="name" placeholder="User name"
                class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('name') border-red-500 @enderror" value="{{ $user->name }}">
                @error('name')
                    <div class="text-red-500 mt-2 text-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div>                   
            <div class="mb-4">
                <label for="email" class="sr-only"> Email </label>
                <input type="text" name="email" id="email" placeholder="User email"
                class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('email') border-red-500 @enderror" value="{{ $user->email }}">
                @error('email')
                    <div class="text-red-500 mt-2 text-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div>            
            

            <div class="mb-4">
                <label for="roles" class=""> Roles </label>
                <div class="">
                    @foreach ($roles as $role)
                        <div class="form-check">
                            <input type="radio" name="roles[]" value="{{ $role->id }}" 
                                {{ $user->role->name == $role->name ? 'checked' : ''  }} >
                            <label> {{ $role->name }} </label>                        
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="mb-4">
                <label for="password" class="sr-only"> Password </label>
                <input type="password" name="password" id="password" placeholder="Enter new Password if change needed"
                class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('password') border-red-500 @enderror" value="">
                @error('password')
                    <div class="text-red-500 mt-2 text-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div> 

            <div class="mb-4">
                <label for="password_confirmation" class="sr-only"> Confirm Password </label>
                <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password"
                class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('password_confirmation') border-red-500 @enderror" value="">
                @error('password_confirmation')
                    <div class="text-red-500 mt-2 text-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <div>
                <button type="submit" class="bg-blue-500 text-white px-4 py-3
                rounded font-medium w-full focus:outline-none hover:bg-indigo-600 rounded"> Update </button>
            </div>           
        </form>
    </div>
</div>
@endsection
