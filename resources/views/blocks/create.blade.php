@extends('layouts.app')

@section('content')

<div class="flex justify-center">
    <div class="w-4/12 bg-white p-6 rounden-lg">
        @if (session('status'))
            <div class="bg-red-500 p-4 rounded-lg mb-6 text-white text-center">
                {{ session('status') }}
            </div>
        @endif

        <div class="mb-4 text-center">
                
        CREATE BLOCK
                
        </div>   

        <form action="{{ route('blocks.index') }}" method="post">
            @csrf
            
            <div class="mb-4">
                <label for="name" class="sr-only"> Name </label>
                <input type="text" name="name" id="name" placeholder="New Block Name"
                class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('name') border-red-500 @enderror" value="{{ old('name') }}">
                @error('name')
                    <div class="text-red-500 mt-2 text-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div>       
            
            <div class="mb-4">
                
                <select name="project" id="project" class="form-control bg-gray-100 border-2 w-full p-4 rounded-lg" required >
                    <option value ="">PROJECT</option>
                    @foreach ($projects as $project)
                        <option value="{{ $project->id }}" > {{$project->name ?? ''}} </option>                                
                    @endforeach
                </select>

            </div>       
            
            <div class="mb-4">
                <label for="etajnost" class="sr-only"> Etajnost </label>
                <input type="text" name="etajnost" id="etajnost" placeholder="Etajnost"
                class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('name') border-red-500 @enderror" value="{{ old('name') }}">
                @error('name')
                    <div class="text-red-500 mt-2 text-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <div>
                <button type="submit" class="bg-blue-500 text-white px-4 py-3
                rounded font-medium w-full focus:outline-none hover:bg-indigo-600 rounded"> Create </button>
            </div>           
        </form>
    </div>
</div>

@endsection
