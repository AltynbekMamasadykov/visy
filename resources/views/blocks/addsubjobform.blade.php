@extends('layouts.app')

@section('content')

<div class="flex justify-center">
    <div class="w-4/12 bg-white p-6 rounden-lg">
        @if (session('status'))
            <div class="bg-red-500 p-4 rounded-lg mb-6 text-white text-center">
                {{ session('status') }}
            </div>
        @endif

        <div class="mb-4 text-center">
                
        ADD SUBJOB TO BLOCK {{$block->name}}
                
        </div>   

        <form action="{{ route('blocks.addsubjob', $block->id) }}" method="post">
            @csrf
            
            <div class="mb-4">
                <label for="block" class="sr-only"> Name </label>
                <input type="text" name="block" id="block" placeholder="New Block Name"
                class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('block') border-red-500 @enderror" value="{{ $block->name }}">
                @error('block')
                    <div class="text-red-500 mt-2 text-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div>       
            
            <div class="mb-4">
                
                <select name="job" id="job" class="form-control bg-gray-100 border-2 w-full p-4 rounded-lg" required >
                    <option value ="">JOB</option>
                    @foreach ($jobs as $job)
                        <option value="{{ $job->id }}" > {{$job->name ?? ''}} </option>                                
                    @endforeach
                </select>

            </div>       
            <div class="mb-4">
                
                <select name="subjob" id="subjob" class="form-control bg-gray-100 border-2 w-full p-4 rounded-lg" required >
                    <option value ="">SUBJOB</option>
                    @foreach ($subjobs as $subjob)
                        <option value="{{ $subjob->id }}" > {{$subjob->name ?? ''}} </option>                                
                    @endforeach
                </select>

            </div>       
            
            
            <div>
                <button type="submit" class="bg-blue-500 text-white px-4 py-3
                rounded font-medium w-full focus:outline-none hover:bg-indigo-600 rounded"> Create </button>
            </div>           
        </form>
    </div>
</div>

@endsection
