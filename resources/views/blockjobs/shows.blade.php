@extends('layouts.app')

@section('content')

    @if (session()->has('message'))
        <div id="alert" class="text-white px-6 py-4 border-0 rounded relative mb-4 bg-green-500 bg-opacity-75">
            <span class="inline-block align-middle mr-8">
                {{ session('message') }}
            </span>
            <button class="absolute bg-transparent text-2xl font-semibold leading-none right-0 top-0 mt-4 mr-6 outline-none focus:outline-none" onclick="document.getElementById('alert').remove();">
                <span>×</span>
            </button>
        </div>
    @endif

    <div class ="flex flex-wrap justify-end text-center mb-6"> 
        <a href="{{ route('blocks.show',$blockjob->block_id)}}">
            <button class="flex mx-auto mt-6 text-white bg-blue-500 border-0 py-2 px-5 focus:outline-none hover:bg-indigo-600 rounded">Back to Jobs</button>
        </a>
    </div>

    <div class="container mx-auto">        
        <p class="text-lg text-center font-bold m-5">{{$blockjob->block_name}}</p>    
    </div>

    <div class="container mx-auto">
            <table class="rounded-t-lg m-5 w-5/6 mx-auto bg-white text-black-200 border-collapse">
            <tr class="text-left border-b border-black-300 bg-blue-500 bg-opacity-75 border text-left px-8 py-4">
                <th class="px-4 py-3">#</th>
                <th class="px-4 py-3">Job</th>
                <th class="px-4 py-3">Subjob</th>
                <th class="px-4 py-3">Status</th>
                <th class="px-4 py-3">Etaj</th>
                <!-- <th class="px-4 py-3">Roles</th> -->
                <th class="px-4 py-3">Start date</th>
                <th class="px-4 py-3">Deadline</th>
                @if (Auth::user()->isAdmin())
                <th class="px-4 py-3">Actions</th>
                @endif
            </tr>      
                
            <tr class="bg-white border-b border-black-600">
                <td class="px-3 py-2 border border-black-600">{{$blockjob->id}}</td>
                <td class="px-3 py-2 border border-black-600"> {{ $blockjob->job_name }}</td>
                <td class="px-3 py-2 border border-black-600"> {{ $blockjob->subjob_name }} </td>
                <td class="px-3 py-2 border border-black-600"> {{ $blockjob->status }}</td>
                <td class="px-3 py-2 border border-black-600"> @isset ($blockjob->etaj) {{ $blockjob->etaj }}  @endisset </td>
                <!-- <td class="px-3 py-2 border border-black-600"> {{ ($blockjob->roles) }} </td> -->
                <td class="px-3 py-2 border border-black-600"> @isset ($blockjob->start_date) {{ $blockjob->start_date->format('d-m-Y') }}  @endisset</td>
                <td class="px-3 py-2 border border-black-600"> @isset ($blockjob->deadline) {{ $blockjob->deadline->format('d-m-Y') }}  @endisset  </td>
                @if (Auth::user()->isAdmin())
                <td class="px-3 py-2 border border-black-600"> 
                    <a href="{{ route ('blockjobs.activate', $blockjob ) }}" class="inline-block" > 
                        <button type="button" class="bg-blue-500 text-white px-2 py-1
                            rounded font-medium w-full focus:outline-none hover:bg-indigo-600 rounded">Activate
                        </button>
                    </a> 
                    <a href="{{ route ('tasks.create', $blockjob ) }}" class="inline-block"> 
                        <button type="button" class="bg-blue-500 text-white px-2 py-1
                            rounded font-medium w-full focus:outline-none hover:bg-indigo-600 rounded">Add Task
                        </button>
                    </a>
                </td>
                @endif
            </tr>    
        </table>
    </div>

    <div class="container mx-auto pb-20 pr-20 pl-20 ">
    <div class="box-border h-100 w-100 p-30 border-4 bg-blue-300 p-20 justify-items-center">
        <!-- ... -->
    
        
        @isset($blockjob->tasks)
            TASKS FOR THIS JOB <br> <hr>
            @foreach($blockjob->tasks as $task)
                <br>
                {{ $task->body }}
                {{ $task->user }}
                {{ $task->created_at->format('d-m-Y') }}
            @if (Auth::user()->isAdmin())
                <a href="{{ route ('tasks.edit', $task->id ) }}" > <button class="bg-blue-500 hover:bg-blue-700 "> Edit Task  </button> </a>
            @endif

                <a href="{{ route ('taskcomments.create', $task->id ) }}"> <button class="bg-blue-500 hover:bg-blue-700 ">Comment </button></a>
                <hr>
                @if(isset($task->comments))
                <ul>                    
                    @foreach($task->comments as $comment)
                        <br>
                        {{$comment->body}} 
                        {{$comment->user}} 
                        {{$comment->created_at->format('d-m-Y')}} 
                        @if(Auth::user()->name == $comment->user)
                        <a href="{{ route ('taskcomments.edit', $comment->id) }}" > <button class="bg-blue-500 hover:bg-blue-700 "> Edit Comment  </button> </a>
                        @endif
                        <hr>
                    @endforeach
                </ul>
                @endif
                
            @endforeach        
        @endisset
        </div>
    </div>



    
@endsection
